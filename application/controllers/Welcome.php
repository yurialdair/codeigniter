<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{


		$this->load->view('layout/head');
		$this->load->view('layout/menu');
		$this->load->view('home');
		$this->load->view('layout/footer');
	}
	public function novocurso(){
		
		$this->load->model('ModelsCategorias','categorias');
		$r = $this->categorias->todos();
		$categorias = array('categorias'=>$r);
		
		$this->load->view('layout/head');
		$this->load->view('layout/menu');
		$this->load->view('cursos/novo',$categorias);
		$this->load->view('layout/footer');
		
	}
	public function novacategoria(){
	
		$this->load->view('layout/head');
		$this->load->view('layout/menu');
		$this->load->view('categoria/novo');
		$this->load->view('layout/footer');
	}

	public function api(){
		$this->load->model('ModelsCursos','cursos');
		$r = $this->cursos->todos();
		// $cursos = array('cursos'=>$r);
		echo $r;
	}

	public function deletar($id){
		
		$this->load->model('ModelsCursos');
        if ($this->ModelsCursos->deletar($id)) {
            
        } else {
            log_message('error', 'Erro ao deletar...');
        }
	}
}
