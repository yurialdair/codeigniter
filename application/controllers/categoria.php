<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class categoria extends CI_Controller {

    public function salvar(){
        $this->load->model('ModelsCategorias');
        $nome = $_POST['nome'];
        $this->ModelsCategorias->nome = $nome;
        $this->ModelsCategorias->inserir();
    }
    
}
