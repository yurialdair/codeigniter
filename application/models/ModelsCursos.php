<?php

class ModelsCursos extends CI_Model{
    public $nome;
    public $id_categoria;

    public function __construct(){
        parent::__construct();
    }
    public function inserir(){
        $dados = array('nome_cur'=>$this->nome,'id_categoria'=>$this->id_categoria); 
        $this->db->insert('cursos',$dados);
        $this->session->set_flashdata('sucesso','Salvo com sucesso');
        return redirect()->to('home');
    }

    public function todos(){
        $this->db->select(['categorias.nome_cat','cursos.nome_cur','cursos.id']);
        $this->db->from('cursos');
        // $this->db->from('categorias');
        $this->db->join('categorias',' categorias.id = cursos.id_categoria','inner');
        $r = $this->db->get()->result();
        return json_encode($r);
    }
    function deletar($id) {
        $this->db->where('id', $id);
        return $this->db->delete('cursos');
    }
    
}