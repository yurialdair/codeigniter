<?php

class ModelsCategorias extends CI_Model{
    public $nome;

    public function __construct(){
        parent::__construct();
    }
    public function inserir(){
        $dados = array('nome_cat'=>$this->nome); 
        $this->db->insert('categorias',$dados);
        $this->session->set_flashdata('sucesso','Salvo com sucesso');
        return redirect()->to('home');
    }

    public function todos(){
        $this->db->select(['nome_cat','id']);
        $r = $this->db->get('categorias')->result();
        return empty($r) ? [] : $r;
    }
}