

</body>
<center>
<footer class="container" style=" position: fixed;bottom:0">
<b>yurialdair@gmail.com</b>
<br>
<?php echo 'codeigniter -v: '.CI_VERSION ?>
</footer>
</center>


<script>

$(function () {

    <?php if($this->session->flashdata('sucesso')){?>
        Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Salvo com sucesso',
            showConfirmButton: false,
            timer: 1500
})
     <?php }?>
    
        $('input[name=search]').keyup(function () {
        
            refresh_search(); 
            
        });
        getContent()
});
    async function getContent() {
        try {
            const response = await fetch("<?php echo site_url('Welcome/api') ?>")
            const data = await response.json()
            show(data)
            refresh_search();
        } catch (error) {
            console.error(error)
        }
    }
    
    function show(users) {
        index = [];
        aux =[]
        output = ''
        users.forEach(function (a, b) {
            aux['curso'] = a.nome_cur;
            aux['categoria'] = a.nome_cat;
            aux['id'] = a.id;
            index.push(aux);
            aux=[];
        });
        refresh_search();
    }


    // Sore the titles in here
index = [];

function fuzzy_match(text, search)
{
    /*
    Text is a title, search is the user's search
    */
    // remove spaces, lower case the search so the search is case insensitive
    var search = search.replace(/\ /g, '').toLowerCase();
    var tokens = [];
    var search_position = 0;

    // Go through each character in the text
    for (var n=0; n<text.length; n++)
    {
        var text_char = text[n];
        // if we watch a character in the search, highlight it
        if(search_position < search.length && text_char.toLowerCase() == search[search_position])
        {
            text_char = '<b>' + text_char + '</b>';
            search_position += 1;
        }
        tokens.push(text_char);
    }
    // If are characters remaining in the search text, return an empty string to indicate no match
    if (search_position != search.length)
    {
        return '';
    }
    return tokens.join('');
}

function refresh_search()
{
    var search = $('input[name=search]').val();
    var results = [];

    /*
        Create an array of <li> tags containing matched titles
    */
    $.each(index, function(i, a){
        
        var result = fuzzy_match(a.curso, search)
        if(result){
            results.push("<tr><td>" + result +"</td><td>" +a.categoria+"</td>");
            results.push('<td><button type="button" class="btn btn-danger btn-sm user_table add" data-user_table-id="'+a.id+'">Deletar</button></td></tr>');
        }
    });

    var results_html = results.join('\n');
    $("tbody.results").html(results_html);
}
$('.users_table').on('click', '.add', function(){
      var documento = $(this).closest('.user_table');
      var id = documento.data('user_table-id');
      confirmar(id);
})


function confirmar(id) {
    $( "#dialog-confirm" ).dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      title:'Deseja Deletar?',
      buttons: {
        "Sim": function() {
          $(this).dialog( "close" );
          $.ajax({
        url: "<?php echo site_url('Welcome/deletar/'); ?>"+id,
        datatype:'json',
        type: 'get',
        }).done(function(data) {
            getContent();
            Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Deletado com sucesso',
            showConfirmButton: false,
            timer: 1500
})
		}).fail(function(jqXHR, textStatus, errorThrown) {
			var json = JSON.parse(jqXHR.responseText);
			swal({
						type: 'error',
						title: 'Ocorreu um erro!',
						text: json.mensagem,
					})
			 
		});
        },
        'Cancelar': function() {
          $( this ).dialog( "close" );
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Cancelado',
            showConfirmButton: false,
            timer: 1500
})
        }
      }
    });
}
</script>


</html>