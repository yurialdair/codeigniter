<div class="container">

<h5><?php echo $this->session->flashdata('erro');?></h5>

<nav class="navbar justify-content-between col">
  <div class="">
  <input type="search" class="form-control" name="search" id="" placeholder="Buscar">
  </div>
  <div class="">
  <a class="btn btn-outline-success my-0 my-sm-0" href="<?php echo site_url('Welcome/novocurso') ?>">+ Curso</a> 
  <a class="btn btn-outline-info my-0 my-sm-0" href="<?php echo site_url('Welcome/novacategoria') ?>">+ Categoria</a> 
  </div>
</nav>

<table class="table table-striped table-hover users_table">
      <thead>
    <tr>
      <th scope="col">Nome</th>
      <th scope="col">Categoria</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <tbody class="results">
</tbody>
  </tbody>
  </table>

  <div id="dialog-confirm"></div>
</div>
