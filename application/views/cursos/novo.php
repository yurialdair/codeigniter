<div class="container">
<form action="<?php echo site_url('cursos/salvar') ?>" method="Post">
  <fieldset>
    <legend>Cursos</legend>
    <div class="mb-3 form-group col-4">
      <label  class="form-label">Nome</label>
      <input type="text" name="nome"  class="form-control col" placeholder="">
    </div>
    <div class="mb-3 form-group col-4">
      <label  class="form-label">Categorias</label>
      <select name="categoria" class="form-control col">
        <option value=""></option>
        <?php 
        foreach($categorias as $cat){
          echo '<option value="'.$cat->id.'">'.$cat->nome_cat.'</option>';
        }
        ?>
      </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </fieldset>
</form>
</div>