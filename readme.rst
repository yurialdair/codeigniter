create table categorias (
id int not null AUTO_INCREMENT,
nome_cat varchar(25) not null, 
primary key(id));

create table cursos (
id int not null AUTO_INCREMENT,
id_categoria int not null,
nome_cur varchar(25) not null, 
primary key(id),
CONSTRAINT fk_categoria_curso FOREIGN KEY (id_categoria) REFERENCES categorias (id));
